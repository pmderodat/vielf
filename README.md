vielf
=====

Because now, you can edit your ELFs with vi!

vielf is a simple tool designed to translate binary ELF files to and from
source files using a textual representation of most ELF built-in structures
(like segment headers, string table sections, relocation tables sections, and
maybe DWARF debugging information one day).

Two executables (Python 3 scripts) are provided:

-   `viunelf`: it takes an ELF file and unpack it into the textual
    representation and binary blobs (like segments content)
-   `vielf`: it turns the textual representation back to the standard ELF
    binary format

vielf aims to be able to handle standard sections (like string tables,
symbol tables, …), but also to be extended to work with custom content (like
non-standard debugging information formats).


About
-----

The tool itself is currently mostly a in-development proof of concept since a
lot of  features are “unstable” or even not started (the textual representation
parsing/output, the extension API, standard sections content decoding, …).

This project idea came from the need to craft custom executable binaries when
creating exercises for CTFs ([Capture The
Flag](https://en.wikipedia.org/wiki/Capture_The_Flag#Computer_security)). Of
course, this tool is not needed to do so, but it may ease the task when
creating multiple exercises!

When more polished, it could be useful for the development of custom
informations in ELF binaries, and could even be used as a back-end for
compilers.
