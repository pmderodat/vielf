#! /usr/bin/env python3

from vielf import VERSION
from setuptools import setup, find_packages

setup(
    name='vielf',
    version=VERSION,
    packages=['vielf'],
    scripts=['tools/vielf', 'tools/viunelf'],
)
