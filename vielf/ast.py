class ELF:

    def __init__(self,
        elfclass, elfdata, elfversion,
        osabi, abiversion,
        elftype,
        machine, entry_point,
        flags, ehsize, phentsize, shentsize,
        shstrndx,
        endbytes
    ):
        self.elfclass = elfclass
        self.elfdata = elfdata
        self.elfversion = elfversion
        self.osabi = osabi
        self.abiversion = abiversion
        self.elftype = elftype
        self.machine = machine
        self.entry_point = entry_point

        self.flags = flags
        self.ehsize = ehsize
        self.phentsize = phentsize
        self.shentsize = shentsize
        self.shstrndx = shstrndx

        self.endbytes = endbytes

        self.segments = []
        self.sections = []


class Segment:

    def __init__(self,
        type, offset, vaddr, paddr, filesz, memsz,
        flags, align,
        endbytes, content
    ):
        self.type = type
        self.offset = offset
        self.vaddr = vaddr
        self.paddr = paddr
        self.filesz = filesz
        self.memsz = memsz
        self.flags = flags
        self.align = align

        self.endbytes = endbytes
        self.content = content


class Section:

    def __init__(self,
        name, type, flags,
        addr, offset, size,
        link, info, addralign, entsize,
        endbytes, content,
        elf, structs,
    ):
        self.name = name
        self.type = type
        self.flags = flags
        self.addr = addr
        self.offset = offset
        self.size = size
        self.link = link
        self.info = info
        self.addralign = addralign
        self.entsize = entsize

        self.endbytes = endbytes
        self.content = content

        self.elf = elf
        self.structs = structs

    def entities(self):
        '''
        Iterate over entities' bytes in the section content.
        '''
        for i in range(self.size // self.entsize):
            start = i * self.entsize
            end = start + self.entsize
            yield self.content[start:end]


class StrtabSection(Section):

    def get(self, index):
        '''
        Return the string located at 'index' in this section.
        '''
        end = index
        while self.content[end] != 0:
            end += 1
        return self.content[index:end]


class Symbol:

    def __init__(self,
        name, info, other, shndx, value, size,
        endbytes, sections
    ):
        self.name = name
        self.info = info
        self.other = other
        self.shndx = shndx
        self.value = value
        self.size = size

        self.endbytes = endbytes
        self.sections = sections

    @property
    def section(self):
        '''
        Return the section pointed by the 'shndx' field.
        '''
        print('{}.shndx = {}'.format(self.name, self.shndx))
        return self.sections[self.shndx]

    @property
    def binding(self):
        return self.info >> 4

    @property
    def type(self):
        return self.info & 0xf

    @property
    def visibility(self):
        return self.other & 0x03

    @classmethod
    def info(self, binding, type):
        return (binding << 4) | (type & 0xf)


class SymtabSection(Section):

    def __init__(self, *args, **kwargs):
        super(SymtabSection, self).__init__(*args, **kwargs)
        self.symbol_struct = self.structs.symbol
        self._symbols = None

    @property
    def strings(self):
        '''
        Return the section that contains symbols' name.
        '''
        return self.elf.sections[self.link]

    @property
    def symbols(self):
        '''
        Lazily unpack the symbol table and return it.

        This process is lazy because the referenced sections (for symbol names,
        or symbol targetted sections) may not be loaded yet at this section's
        initialization time.
        '''
        # Return the symbols table if already initialized.
        if self._symbols is not None:
            return self._symbols

        # ... or initialize it.
        _symbols = self._symbols = []

        struct_size = self.symbol_struct.size
        strings = self.strings

        for entity_bytes in self.entities():
            symbol_bytes = entity_bytes[:struct_size]
            endbytes = entity_bytes[struct_size:]
            symbol = self.symbol_struct.unpack(symbol_bytes)
            _symbols.append(Symbol(
                strings.get(symbol.name),
                symbol.info,
                symbol.other,
                symbol.shndx,
                symbol.value,
                symbol.size,
                endbytes, self.elf.sections
            ))

        return self._symbols


class RelSection(Section):

    def __init__(*args, **kwargs):
        super(RelSection, self).__init__(*args, **kwargs)

    @property
    def count(self):
        return self.size // self.entsize

    def get_entry(self, index):
        pass # TODO
