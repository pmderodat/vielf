from .utils import elf_enums, elf_structs


class ELFSerializer:

    def __init__(self, ast, stream):
        self.indent_block = ' ' * 4
        self.indent_level = 0
        self.ast = ast
        self.stream = stream


    #
    # Output formatting utilities
    #

    def emit(self, *args, **kwargs):
        kwargs['file'] = self.stream
        print(self.indent_block * self.indent_level, end='', file=self.stream)
        print(*args, **kwargs)

    def indent(self):
        class IndentationBlock:
            def __enter__(self_):
                self.indent_level += 1
                return self_
            def __exit__(self_, type, value, traceback):
                self.indent_level -= 1
            def __call__(self_, *args, **kwargs):
                self.emit(*args, **kwargs)
        return IndentationBlock()


    #
    # Serialization logic
    #

    def serialize(self):
        self.dispatch(self.ast)

    def dispatch(self, node):
        handler_name = 'handle_{}'.format(type(node).__name__.lower())
        try:
            handler = getattr(self, handler_name)
        except AttributeError:
            raise ValueError('Invalid AST node: {}'.format(node)) from None
        return handler(node)


    def handle_elf(self, elf):
        self.emit('elf')
        with self.indent() as emit:
            emit('class',       elf_enums.ArchClass[elf.elfclass])
            emit('data',        elf_enums.ArchData[elf.elfdata])
            emit('version',     elf.elfversion)
            emit('osabi',       elf_enums.OsAbi[elf.osabi])
            emit('abiversion',  elf.abiversion)
            emit('type',        elf_enums.ElfType[elf.elftype])
            emit('machine',     elf_enums.Architecture[elf.machine])
            emit('entry',       self.pointer_to_str(elf.entry_point))
            emit('flags',       hex(elf.flags))
            emit('phentsize',   hex(elf.phentsize)),
            emit('shentsize',   hex(elf.shentsize)),
            emit('shstrndx',    elf.shstrndx)
            self.emit_endbytes(elf.endbytes)

        self.emit('segments')
        with self.indent() as emit:
            for segment in elf.segments:
                self.dispatch(segment)

        self.emit('sections')
        with self.indent() as emit:
            for section in elf.sections:
                self.dispatch(section)

    def handle_segment(self, segment):
        self.emit(self.enum_to_str(segment.type, elf_enums.SegmentType))

        with self.indent() as emit:
            emit('address',
                self.pointer_to_str(segment.vaddr),
                self.pointer_to_str(segment.paddr),
            )
            emit('size',        hex(segment.memsz))
            emit('flags',
                self.flags_to_str(segment.flags, elf_enums.SegmentFlags))
            emit('align',       hex(segment.align))
            self.emit_endbytes(segment.endbytes)
            # TODO: emit content to an external file


    def handle_basic_section(self, section):
        self.emit(
            self.enum_to_str(section.type, elf_enums.SectionType),
            self.bytes_to_str(section.name)
        )

        with self.indent() as emit:
            emit('flags',
                self.flags_to_str(section.flags, elf_enums.SectionFlags))
            emit('address',     self.pointer_to_str(section.addr))
            emit('link',        section.link)
            emit('info',        hex(section.info))
            emit('addralign',   hex(section.addralign))
            emit('entsize',     hex(section.entsize))
            self.emit_endbytes(section.endbytes)

    def handle_section(self, section):
        return self.handle_basic_section(section)

    def handle_strtabsection(self, section):
        return self.handle_basic_section(section)

    def handle_symtabsection(self, section):
        self.handle_basic_section(section)
        with self.indent() as emit:
            self.emit('symbols')
            for symbol in section.symbols:
                self.handle_symbol(symbol)

    def handle_symbol(self, symbol):
        with self.indent() as emit:
            emit(
                '{visibility} {binding} {type} '
                '{name}{size} {value} in {section}'.format(**{
                    'visibility': self.enum_to_str(
                        symbol.visibility, elf_enums.SymbolVisibility,
                        justify=True
                    ).ljust(10),
                    'binding': self.enum_to_str(
                        symbol.binding, elf_enums.SymbolBinding,
                        justify=True
                    ),
                    'type': self.enum_to_str(
                        symbol.type, elf_enums.SymbolType,
                        justify=True
                    ),
                    'name': (
                        self.bytes_to_str(symbol.name)
                        if symbol.name else
                        'noname'
                    ),
                    'size': (
                        '[{:#x}]'.format(symbol.size)
                        if symbol.size else ''
                    ),
                    'value': self.pointer_to_str(symbol.value),
                    'section': self.section_to_str(symbol.shndx),
                })
            )
            self.emit_endbytes(symbol.endbytes)


    #
    # Serialization utils
    #

    def enum_to_str(self, value, enum_type, justify=False):
        try:
            result = enum_type[value]
        except KeyError:
            result = 'unknown {:x}'.format(value)
        if justify:
            result = result.ljust(enum_type.name_maxsize + 1)
        return result

    def pointer_to_str(self, pointer):
        formatting = {
            elf_enums.ArchClass.dict['32bits']: '{:#010x}',
            elf_enums.ArchClass.dict['64bits']: '{:#018x}',
        }[self.ast.elfclass]
        return formatting.format(pointer)

    def bytes_to_str(self, bytes_):
        return repr(bytes(bytes_).decode('latin-1'))

    def flags_to_str(self, flags, type):
        flags = type.unpack(flags)
        if flags:
            result = ' | '.join(
                hex(flag) if isinstance(flag, int) else flag
                for flag in flags
            )
            return (
                '({})'.format(result)
                if len(flags) > 1 else
                result
            )
        else:
            return '0'

    def section_to_str(self, shndx):
        special = elf_enums.SectionIndices
        if shndx == special.UNDEF:
            return 'undefined'
        elif shndx == special.ABS:
            return 'absent'
        elif shndx == special.COMMON:
            return 'common'
        elif shndx == special.XINDEX:
            return 'extra'
        elif special.LORESERVE <= shndx <= special.HIRESERVE:
            return 'reserved {:#04x}'.format(shndx)
        else:
            return self.bytes_to_str(self.ast.sections[shndx].name)

    def emit_endbytes(self, endbytes):
        if endbytes:
            self.emit('endbytes',   self.bytes_to_str(endbytes))
