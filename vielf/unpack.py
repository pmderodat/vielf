from .ast import (
    ELF,
    Segment,
    Section, StrtabSection, SymtabSection,
)
from .utils.struct import Architecture
from .utils import elf_enums, elf_structs


class LazySectionName:

    def __init__(self, elf, strndx):
        self.elf = elf
        self.strndx = strndx

    def __bytes__(self):
        name_section = self.elf.sections[self.elf.shstrndx]
        return name_section.get(self.strndx)


class ELFUnpacker:

    def __init__(self, stream):
        self.stream = stream

    def unpack(self):
        # First, unpack the 'ident' bytes array.
        ident = elf_structs.elf_header_ident.read(self.stream)
        if ident.magic != elf_enums.MAGIC:
            raise ValueError(
                'Invalid ELF magic number: {}'.format(repr(ident.magic)))

        # Deduce the architecture, and then the ELF structures from it.
        arch = Architecture(ident.elf_class, ident.elf_data)
        structs = self.structs = elf_structs.get_structs(arch)

        # Unpack the ELF header.
        self.stream.seek(0)
        header = structs.elf_header.read(self.stream)

        # ... and padding bytes
        end_size = max(0, header.ehsize - structs.elf_header.size)
        endbytes = self.stream.read(end_size)

        # Build the ELF AST node.
        elf = ELF(
            ident.elf_class, ident.elf_data, ident.elf_version,
            ident.osabi, ident.abiversion,
            header.type,
            header.machine, header.entry_point,
            header.flags, header.ehsize, header.phentsize, header.shentsize,
            header.shstrndx,
            endbytes
        )

        for i in range(header.phnum):
            prog_header = structs.program_header.read(
                self.stream,
                offset=header.phoff + header.phentsize * i
            )
            end_size = max(0, header.phentsize - structs.program_header.size)
            endbytes = self.stream.read(end_size)
            elf.segments.append(
                self.unpack_segment(prog_header, endbytes)
            )

        for i in range(header.shnum):
            section_header = structs.section_header.read(
                self.stream,
                offset=header.shoff + header.shentsize * i
            )
            end_size = max(0, header.shentsize - structs.section_header.size)
            endbytes = self.stream.read(end_size)
            elf.sections.append(
                self.unpack_section(elf, section_header, endbytes)
            )

        return elf


    def unpack_segment(self, prog_header, endbytes):
        self.stream.seek(prog_header.offset)
        content = self.stream.read(prog_header.filesz)
        return Segment(
            prog_header.type, prog_header.offset,
            prog_header.vaddr, prog_header.paddr,
            prog_header.filesz, prog_header.memsz,
            prog_header.flags, prog_header.align,
            endbytes, content
        )

    def unpack_section(self, elf, section_header, endbytes):
        if section_header.type == elf_enums.SectionType.NOBITS:
            content = None
        else:
            self.stream.seek(section_header.offset)
            content = self.stream.read(section_header.size)

        section_class_dispatcher = {
            elf_enums.SectionType.STRTAB: StrtabSection,
            elf_enums.SectionType.SYMTAB: SymtabSection,
        }
        section_class = section_class_dispatcher.get(
            section_header.type,
            Section
        )

        return section_class(
            LazySectionName(elf, section_header.name),
            section_header.type, section_header.flags,
            section_header.addr, section_header.offset, section_header.size,
            section_header.link, section_header.info,
            section_header.addralign, section_header.entsize,
            endbytes, content,
            elf, self.structs,
        )
