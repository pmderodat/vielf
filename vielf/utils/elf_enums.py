from .enum import Enum, Flags

MAGIC = b'\x7fELF'

ArchClass = Enum(none=0, **{'32bits': 1, '64bits': 2})
ArchData = Enum(none=0, lsb=1, msb=2)

ElfVersion = Enum(none=0, current=1)
ElfType = Enum(
    none=0,
    rel=1,
    exec=2,
    dyn=3,
    core=4,
    loos_=0xfe00,
    hios_=0xfeff,
    loproc_=0xff00,
    hiproc_=0xffff,
)

OsAbi = Enum(
    sysv=0,
    hpux=1,
    netbsd=2,
    linux=3,
    hurd=4,
    solaris=5,
    aix=7,
    irix=8,
    freebsd=9,
    tru64=10,
    modesto=11,
    openbsd=12,
    openvms=13,
    nsk=14,
    aros=15,
    arm_aeabi=64,
    arm=97,
    standalone=255,
)

Architecture = Enum(
    none=0,
    m32=1,
    sparc=2,
    i386=3,
    m68k=4,
    m88k=5,
    m860=7,
    mips=8,
    ia_64=50,
    x86_64=62,
    avr=83,
    l10m=180,
    # TODO: transcript a lot of values from elf.h
)

SegmentType = Enum(
    null            = 0,
    load            = 1,
    dynamic         = 2,
    interp          = 3,
    note            = 4,
    shlib           = 5,
    phdr            = 6,
    tls             = 7,
    num             = 8,

    loos_           = 0x60000000,

    gnu_eh_frame    = 0x6474e550,
    gnu_stack       = 0x6474e551,
    gnu_relro       = 0x6474e552,

    losunw_         = 0x6ffffffa,
    sunwbss         = 0x6ffffffa,
    sunwstack       = 0x6ffffffb,
    hisunw_         = 0x6fffffff,

    hios_           = 0x6fffffff,
    loproc_         = 0x70000000,
    hiproc_         = 0x7fffffff,
)

SegmentFlags = Flags(
    x           = 1 << 0,
    w           = 1 << 1,
    r           = 1 << 2,
    maskos_     = 0x0ff00000,
    maskproc_   = 0xf0000000,
)

SectionIndices = Enum(
    undef       = 0,

    loreserve_  = 0xff00,

    loproc_     = 0xff00,
    before_     = 0xff00,
    after_      = 0xff01,
    hiproc_     = 0xff1f,

    loos_       = 0xff20,
    hios_       = 0xff3f,

    abs         = 0xfff1,
    common      = 0xfff2,

    xindex      = 0xffff,
    hireserve_  = 0xffff,
)

SectionType = Enum(
    null            = 0,
    progbits        = 1,
    symtab          = 2,
    strtab          = 3,
    rela            = 4,
    hash            = 5,
    dynamic         = 6,
    note            = 7,
    nobits          = 8,
    rel             = 9,
    shlib           = 10,
    dynsym          = 11,
    init_array      = 14,
    fini_array      = 15,
    preint_array    = 16,
    group           = 17,
    symtab_shndx    = 18,
    num             = 19,

    loos_           = 0x60000000,

    gnu_attributes  = 0x6ffffff5,
    gnu_hash        = 0x6ffffff6,
    gnu_liblist     = 0x6ffffff7,
    checksum        = 0x6ffffff8,

    losunw_         = 0x6ffffffa,
    sunw_move       = 0x6ffffffa,
    sunw_comdat     = 0x6ffffffb,
    sunw_syminfo    = 0x6ffffffc,
    gnu_verdef      = 0x6ffffffd,
    gnu_versym      = 0x6fffffff,
    hisunw_         = 0x6fffffff,

    hios_           = 0x6fffffff,
    loproc_         = 0x70000000,
    hiproc_         = 0x7fffffff,
    louser_         = 0x80000000,
    hiuser_         = 0x8fffffff,
)

SectionFlags = Flags(
    write               = 1 << 0,
    alloc               = 1 << 1,
    execinstr           = 1 << 2,
    merge               = 1 << 4,
    strings             = 1 << 5,
    info_link           = 1 << 6,
    link_order          = 1 << 7,
    os_nonconforming    = 1 << 8,
    group               = 1 << 9,
    tls                 = 1 << 10,

    maskos_             = 0x0ff00000,
    maskproc_           = 0xf0000000,
    ordered             = 1 << 30,
    exclude             = 1 << 31,
)

SymbolBinding = Enum(
    local               = 0,
    # global is a keyword: it is in the arguments dictionary
    weak                = 2,

    num_                = 3,
    loos_               = 10,
    gnu_unique          = 10,
    hios_               = 12,

    loproc_             = 13,
    hiproc_             = 15,
    **{'global':          1}
)

SymbolType = Enum(
    notype              = 0,
    object              = 1,
    func                = 2,
    section             = 3,
    file                = 4,
    common              = 5,
    tls                 = 6,

    num_                = 7,
    loos_               = 10,
    gnu_ifunc           = 10,
    hios_               = 12,

    loproc_             = 13,
    hiproc_             = 15,
)

SymbolVisibility = Enum(
    default             = 0,
    internal            = 1,
    hidden              = 2,
    protected           = 3,
)
