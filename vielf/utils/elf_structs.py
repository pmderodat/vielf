from collections import namedtuple

from .elf_enums import MAGIC, ArchClass, ArchData
from .struct import Architecture, Struct, Field


# The default arch is used to read the first part of the ELF header: it is
# built using unsigned bytes, so the architecture does not matter for it.
default_arch = Architecture(ArchClass.dict['32bits'], ArchData.LSB)
elf_header_ident = default_arch.Struct('HeaderIdent',
    Field('magic',          'string', 4),
    Field('elf_class',      'byte'),
    Field('elf_data',       'byte'),
    Field('elf_version',    'byte'),
    Field('osabi',          'byte'),
    Field('abiversion',     'byte'),
    Field('padding',        'string', 7),
)


ELFStructs = namedtuple('ELFStructs', (
    'elf_header',
    'program_header',
    'section_header',
    'symbol',
))

def get_structs(arch):
    is_32bits = arch.archclass == ArchClass.dict['32bits']
    int_t = 'u32' if is_32bits else 'u64'

    elf_header = arch.Struct('ELFHeader',
        Field('magic',          'string', 4),
        Field('elf_class',      'byte'),
        Field('elf_data',       'byte'),
        Field('elf_version',    'byte'),
        Field('osabi',          'byte'),
        Field('abiversion',     'byte'),
        Field('padding',        'string', 7),

        Field('type',           'u16'),
        Field('machine',        'u16'),
        Field('elfversion_',    'u32'),
        Field('entry_point',    'addr'),
        Field('phoff',          'off'),
        Field('shoff',          'off'),
        Field('flags',          'u32'),
        Field('ehsize',         'u16'),
        Field('phentsize',      'u16'),
        Field('phnum',          'u16'),
        Field('shentsize',      'u16'),
        Field('shnum',          'u16'),
        Field('shstrndx',       'u16'),
    )

    # Program header structure
    # The two differences between 32 bits and 64 bits is 1) the position of the
    # 'flags' field and the size of some fields (respectively 32 and 64 bits).
    ph_fields = (
        Field('type',           'u32'),
    )
    if not is_32bits:
        ph_fields += (Field('flags', 'u32'), )
    ph_fields += (
        Field('offset',         'off'),
        Field('vaddr',          'addr'),
        Field('paddr',          'addr'),
        Field('filesz',         int_t),
        Field('memsz',          int_t),
    )
    if is_32bits:
        ph_fields += (Field('flags', 'u32'), )
    ph_fields += (
        Field('align',          int_t),
    )
    program_header = arch.Struct('ELFProgramHeader', *ph_fields)

    # Section header structure
    section_header = arch.Struct('ELFSectionHeader',
        Field('name',           'u32'),
        Field('type',           'u32'),
        Field('flags',          int_t),
        Field('addr',           'addr'),
        Field('offset',         'off'),
        Field('size',           int_t),
        Field('link',           'u32'),
        Field('info',           'u32'),
        Field('addralign',      int_t),
        Field('entsize',        int_t),
    )

    # Symbol structure
    symbol_fields = (
        Field('name',           'u32'),
    )
    symbol_value_size_fields = (
        Field('value',          'addr'),
        Field('size',           int_t),
    )
    symbol_other_fields = (
        Field('info',           'byte'),
        Field('other',          'byte'),
        Field('shndx',          'u16'),
    )
    if is_32bits:
        symbol_fields += (
            symbol_value_size_fields +
            symbol_other_fields
        )
    else:
        symbol_fields += (
            symbol_other_fields +
            symbol_value_size_fields
        )
    symbol = arch.Struct('ELFSym', *symbol_fields)

    return ELFStructs(
        elf_header, program_header, section_header,
        symbol,
    )
