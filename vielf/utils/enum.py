class Enum:
    '''
    Set of possible values for a type, just like a C enum. Meta-items (that
    cannot be used to translate values back to strings) must end with an
    underscore.
    '''

    def __init__(self, **kwargs):
        # Mapping constant name -> constant value
        self.dict = dict(
            ((name[:-1] if name[-1:] == '_' else name), value)
            for name, value in kwargs.items()
        )

        # Mapping constant value -> constant name
        self.reverse_dict = {}
        for name, value in kwargs.items():
            if (
                name[-1:] != '_' and
                self.reverse_dict.setdefault(value, name) != name
            ):
                raise ValueError('{} and {} have the same value ({})'.format(
                    name, self.reverse_dict[value], value))

        self.name_maxsize = max(len(name) for name, _ in kwargs.items())

    def __getattr__(self, name):
        try:
            return self.dict[name.lower()]
        except KeyError:
            return super(Enum, self).__getattr__(name)

    def __getitem__(self, value):
        return self.reverse_dict[value]

    def is_in_range(self, value):
        return value in self.reverse_dict


class Flags(Enum):
    '''
    Set of possible flags for a type.
    '''

    def pack(self, *args, allow_unknown=True):
        result = 0
        for arg in args:
            if not allow_unknown and arg not in self.reverse_dict:
                raise ValueError('Out of range flag: {}'.format(arg))
            result |= arg
        return result

    def unpack(self, value, allow_unknown=True):
        result = []
        handled_bits = 0
        for key, flag in self.dict.items():
            if value & flag:
                result.append(key)
                handled_bits |= flag
        unhandled_bits = value & ~handled_bits
        if unhandled_bits:
            if allow_unknown:
                result.append(unhandled_bits)
            else:
                raise ValueError(
                    'Out of range flag: {}'.format(unhandled_bits))
        return result

    def is_in_range(self, value):
        try:
            self.unpack(value, allow_unknown=False)
        except ValueError:
            return False
        return True
