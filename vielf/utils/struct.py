from collections import namedtuple
import struct

from . import elf_enums
from .enum import Enum


def Field(name, kind, count=1):
    return (name, kind, count)


class Struct(struct.Struct):

    field_kinds = {
        elf_enums.ArchClass.dict['32bits']: {
            'addr': 'I',
            'off': 'I',
            'section': 'H',
            'versym': 'H',
            'u16': 'H',
            'u32': 'I',
            'u64': 'Q',
            'string': 's',
            'byte': 'B',
            'half': 'H',
            'sword': 'i',
            'word': 'I',
            'sxword': 'q',
            'xword': 'Q',
        },
        elf_enums.ArchClass.dict['64bits']: {
            'addr': 'Q',
            'off': 'Q',
            'section': 'H',
            'versym': 'H',
            'u16': 'H',
            'u32': 'I',
            'u64': 'Q',
            'string': 's',
            'byte': 'B',
            'half': 'H',
            'sword': 'i',
            'word': 'I',
            'sxword': 'q',
            'xword': 'Q',
        },
    }

    def __init__(self, name, archclass, archdata, *fields):
        endianess = {
            elf_enums.ArchData.LSB: '<',
            elf_enums.ArchData.MSB: '>',
        }[archdata]
        field_kinds = self.field_kinds[archclass]
        struct_fmt = endianess
        for _, kind, count in fields:
            if count != 1:
                struct_fmt += str(count)
            try:
                struct_fmt += field_kinds[kind]
            except KeyError:
                raise ValueError('Invalid datum type: {}'.format(kind))

        self.tuple = namedtuple(name, [field[0] for field in fields])

        super(Struct, self).__init__(struct_fmt)

    def unpack(self, buffer):
        return self.tuple(*super(Struct, self).unpack(buffer))

    def read(self, stream, offset=None):
        if offset is not None:
            stream.seek(offset)
        buffer = b''
        while len(buffer) != self.size:
            read_bytes = stream.read(self.size - len(buffer))
            if len(read_bytes) == 0:
                raise EOFError()
            buffer += read_bytes
        return self.unpack(buffer)

class Architecture:

    def __init__(self, archclass, archdata):
        self.archclass = archclass
        self.archdata = archdata

    def Struct(self, name, *fields):
        return Struct(name, self.archclass, self.archdata, *fields)
